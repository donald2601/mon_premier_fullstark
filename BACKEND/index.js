const jwt = require('jsonwebtoken')

//EXPRESS
import express from 'express'
const app = express();
app.use(express.urlencoded({extended:true}))
app.use(express.json());

//DOTENV
require('dotenv').config();

//UTILISATEUR
const user = {
    id: 2,
    name : "donald",
    email: 'donald24@gmail.com',
    password : 'super',
    admin : true
}

//GENERATION DE TOKEN
const generateToken = (user)=>{
 return jwt.sign(user,process.env.ACCESS_TOKEN_SECRET, {expiresIn:'1800s'});
}



//SERVER-ROUTE
app.post('/api/login',(req,res)=>{
    if (req.body.email !== user.email && req.body.password !== user.password) {
        res.status(401).send('entre invalide')
    }
    const accessToken = generateToken(user);
    res.send({accessToken});
})
app.listen(3000, ()=>{
    console.log('server pret ');
})